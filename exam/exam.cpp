#include "exam.hh"
#include "qr.hh"


double NewtonsMethod(vec f(vec &u, vec &d, vec x), vec &u, vec &d, vec &x, double stepsize, double tolerence, vec &x_upper, vec &x_lower, int &counts){
	counts++; //For allowing trying again if wrong value found
	int non_conv=0; //To stop if it doesn't find any roots. Mainly relevent for part C
	int n = x.n_elem;//dimensions
	int cont = 1; //continue?
	mat J(n,n); //for jacobian
	mat R(n,n); //for QR decom
	vec Dx(n);  //for QR solve
	vec y(n), fy(n), df(n), fx(n); 
		



	while(cont==1){
		non_conv++;
		if(non_conv > 50){cont=0;}
		fx=f(u,d,x);
		//J-matrix calculated
		for(int j=0;j<n;j++){
			x(j)+=stepsize;
			df=f(u,d,x)-fx;
			for(int i=0;i<n;i++){
				J(i,j)=df(i)/stepsize;
			}
			x(j)-=stepsize;
		}
	

		//Solve J Dx = -f(x) as in eq 5.
		vec minus_fx = (-1)*fx;
		class qr_gs qr(J,R); //decomposition to QR
		qr.solve(J,R,minus_fx,Dx); //solving


		//This seems to be the backtracking part
		double bt = 2;
		int    bt_cont = 1;
		while(bt_cont==1){
			bt/=2.0;
			y=x+Dx*bt;
			fy=f(u,d,y);
			if(norm(fy)<((1.0-bt/2.0)*norm(fx)) || bt<0.02){bt_cont=0;}
			}
		x=y;
		fx=fy;		
		if(norm(Dx)<stepsize || norm(fx)<tolerence){cont=0;}
	}
	if(x(0)<x_lower(0) && counts < 10){ //If eigenvalue is not within interval we try again searching in both lower half and upper half from the middle point. Hopefully this wont need too many iterations. Other methods seems to go into infinite loops.
		vec xlow=(3.0*x_lower+1.0*x_upper)/4.0; //This is not good code, but newtons method has a habit of mssing some root... This catches a few extra points.
		vec xmid = (x_lower+x_upper)/2.0;
		x(0)=NewtonsMethod(f,u,d,xlow,stepsize,tolerence,xmid,x_lower,counts);
	}
	else if(x(0)>x_upper(0) && counts < 10){
		vec xhigh=(3.0*x_upper+1.0*x_lower)/4.0;
		vec xmid = (x_lower+x_upper)/2.0;
		x(0)=NewtonsMethod(f,u,d,xhigh,stepsize,tolerence,x_upper,xmid,counts);
	}
	return x(0);
}

double NewtonsMethod_jac(vec f(vec &u, vec &d, vec x), mat j(vec &u, vec &d, vec x), vec &u, vec &d, vec &x, double stepsize, double tolerence, vec &x_upper, vec &x_lower, int &counts){
	counts++;
	int non_conv = 0;
	int n = x.n_elem;//dimensions
	int cont = 1; //continue?
	mat J(n,n); //for jacobian
	mat R(n,n); //for QR decom
	vec Dx(n);  //for QR solve
	vec y(n), fy(n), df(n), fx(n); 
		



	while(cont==1){
		non_conv++;
		if(non_conv > 100){cont = 0;}
		fx=f(u,d,x);
		J =j(u,d,x);

		//Solve J Dx = -f(x) as in eq 5.
		vec minus_fx = (-1)*fx;
		class qr_gs qr(J,R); //decomposition to QR
		qr.solve(J,R,minus_fx,Dx); //solving


		//This seems to be the backtracking part
		double bt = 2;
		int    bt_cont = 1;
		while(bt_cont==1){
			bt/=2.0;
			y=x+Dx*bt;
			fy=f(u,d,y);
			if(norm(fy)<((1.0-bt/2.0)*norm(fx)) || bt<0.02){bt_cont=0;}
			}
		x=y;
		fx=fy;		
		if(norm(Dx)<stepsize || norm(fx)<tolerence){cont=0;}
	}
	if(x(0)<x_lower(0) && counts < 10){ //If eigenvalue is not within interval we try again searching in both lower half and upper half from the middle point. Hopefully this wont need too many iterations. Other methods seems to go into infinite loops.
		vec xlow=(3.0*x_lower+1.0*x_upper)/4.0; //This is not good code, but newtons method has a habit of mssing some root... This catches a few extra points.
		vec xmid = (x_lower+x_upper)/2.0;
		x(0)=NewtonsMethod(f,u,d,xlow,stepsize,tolerence,xmid,x_lower,counts);
	}
	else if(x(0)>x_upper(0) && counts < 10){
		vec xhigh=(3.0*x_upper+1.0*x_lower)/4.0;
		vec xmid = (x_lower+x_upper)/2.0;
		x(0)=NewtonsMethod(f,u,d,xhigh,stepsize,tolerence,x_upper,xmid,counts);
	}
	return x(0);
}




//Brents method. This should be more robust than the above... Also, it includes both bisection method and quadratic interpolation. Theory is described in the wiki article on the method



vec Brent(vec f(vec &u, vec &d, vec x), vec &u, vec &d, double upper, double lower, double tolerence, double max_iterations){

	double div_add = 1e-12; //Because the function diverges at bounds:
	vec s(1);
	s(0)={0.0};
	vec a = {lower + div_add};
	vec b = {upper - div_add};
	vec fa = {f(u,d,a)};
	vec fb = {f(u,d,b)};
	vec fs(1);
	if(fa(0)*fb(0)>0){
		cout << "\nThe bounds have the same sign, doofus...";
		return s;
	}
	if(norm(fa)<norm(fb)){
		swap(a,b);
		swap(fa,fb);
	}
	
	int mflag = 1; //Decides whether to use bisection in a statement later on;
	vec d_brent(1); //For later
	vec c = a;
	vec fc = fa; //Starting point of c
	int iter = 0;
	while(norm(b-a)>tolerence){
		iter++;
		if(iter > max_iterations){cout << endl << endl << "Too many iters" << endl << endl; break;}	
		
		if(fa(0) != fc(0) && fb(0) != fc(0)){ //Inverse quadratic interpolation
			s = a*fb*fc/((fa-fb)*(fa-fc))+b*fa*fc/((fb-fa)*(fb-fc))+c*fa*fb/((fc-fa)*(fc-fb));
		}
		else{
			s = b-fb*(b-a)/(fb-fa);
		} //Secant
		
		//The following is the long cnodition list from the wiki
		if( ( s(0) < (3*a(0)+b(0))/4.0 || (s(0)>b(0))) ||
		    (mflag=1 && norm(s-b) >= (norm(b-c)/2.0)) ||
	    	    (mflag=0 && norm(s-b) >= (norm(c-d)/2.0)) ||
		    (mflag=1 && norm(b-c) < tolerence) ||
		    (mflag=0 && norm(c-d) < tolerence) ){
		
			s=(a+b)/2.0; //Bisection
			mflag=1;
		}
		else{mflag=0;}
		fs = f(u,d,s);
		
		d_brent = c;
		c = b;
		fc = fb;
		if(fa(0)*fs(0) < 0){
			b=s;
			fb = fs;
		}
		else{
			a=s;
			fa=fs;
		}

		if(norm(fa)<norm(fb)){
			swap(a,b);
			swap(fa,fb);
		}

	}

	return s;
}





