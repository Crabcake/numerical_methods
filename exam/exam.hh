#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>
#include <iomanip>
#ifndef exam_guard
#define exam_guard

using namespace std;
using namespace arma;

double NewtonsMethod(vec f(vec &u, vec &d, vec x), vec &u, vec &d, vec &x, double stepsize, double tolerence, vec &x_upper, vec &x_lower, int &counts);



double NewtonsMethod_jac(vec f(vec &u, vec &d, vec x), mat j(vec &u, vec &d, vec x), vec &u, vec &d, vec &x, double stepsize, double tolerence, vec &x_upper, vec &x_lower, int &counts);



vec Brent(vec f(vec &u, vec &d, vec x), vec &u, vec &d, double upper, double lower, double tolerence, double max_iterations);

#endif
