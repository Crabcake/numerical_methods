#include "exam.hh"
int f_calls = 0;
vec secular_equation(vec &u, vec &d, vec X){
	double x = X(0);
	f_calls++;
	int n = u.n_elem;
	vec fX(1);
	fX(0) = 1;
	for(int i = 0; i<n; i++){
		fX(0)+=u(i)*u(i)/(d(i)-x); //The equation described
	}
	return fX;
}

mat secular_equation_jac(vec &u, vec &d, vec X){
	f_calls++;
	double x = X(0);
	int n = u.n_elem;
	mat J(1,1);
	J(0,0) = 0;
	for(int i = 0; i<n; i++){
		J(0,0)+=u(i)*u(i)/((d(i)-x)*(d(i)-x));
	}
	return J;
}




int main(){

ofstream dataA;
dataA.open("out.A.txt");
dataA << "Exam part A" << endl << endl;

//Create the system we want to investigate:
int n = 7;
mat D(n,n,fill::zeros);
D.diag() = randu<vec>(n)*n; //Nicer numbers
vec d = D.diag();
vec u=randu<vec>(n);
dataA << "This is D" << endl << endl << D << endl << endl << "This is u" << endl << endl <<  u << endl << endl;

//In this case sigma=1 thus simplifying our problem slightly compared to the books description.
//The secular equation is inserted at the top. Now we just need to find roots within given
//Intervals as described in equations 21/22

//--------Using newtons method without the jacobian:--------//

vec d_sorted = sort(d); //Sorted version for eigenvalue search
vec search_start(n); //For the start guesses
vec search_lower(n); //For the lower limit of i'th eigenvalue
vec search_upper(n); //For the upper limit of the 'ith eigenvalue
vec eigen(n); //Container for eigenvalues
for(int i = 0; i < n-1; i++){ 
	search_start(i) = (d_sorted(i+1)+d_sorted(i))/2.0;
	search_lower(i) = d_sorted(i);
	search_upper(i) = d_sorted(i+1);

}
search_start(n-1) = (2.0*d_sorted(n-1)+dot(u,u))/2.0;
search_lower(n-1)  = d_sorted(n-1);
search_upper(n-1)  = d_sorted(n-1)+dot(u,u);

double tolerence = 1e-9, stepsize = 1e-5;
int runs;
vec startX(n), start_lower(n), start_upper(n);
f_calls = 0;
for (int i = 0; i<n; i++){
	startX = {search_start(i)};
	start_lower = {search_lower(i)};
	start_upper = {search_upper(i)};
	runs = 0;
	eigen(i) = NewtonsMethod(secular_equation, u, d, startX, stepsize, tolerence, start_upper, start_lower, runs);
	//Check solutions:
}
dataA << "Roots found using newtons method without jacobian" << endl << eigen << endl;
dataA << "Completed using: " << f_calls << " function calls" << endl << endl;




f_calls = 0;
vec search_start_jac(n); //For the start guesses
vec search_lower_jac(n); //For the lower limit of i'th eigenvalue
vec search_upper_jac(n); //For the upper limit of the 'ith eigenvalue
vec eigen_jac(n); //Container for eigenvalues
for(int i = 0; i < n-1; i++){ 
	search_start_jac(i) = (d_sorted(i+1)+d_sorted(i))/2.0;
	search_lower_jac(i) = d_sorted(i);
	search_upper_jac(i) = d_sorted(i+1);

}
search_start_jac(n-1)  = (2.0*d_sorted(n-1)+dot(u,u))/2.0;
search_lower_jac(n-1)  = d_sorted(n-1);
search_upper_jac(n-1)  = d_sorted(n-1)+dot(u,u);

for (int i = 0; i<n; i++){
	startX = {search_start_jac(i)};
	start_lower = {search_lower_jac(i)};
	start_upper = {search_upper_jac(i)};
	runs = 0;
	eigen_jac(i) = NewtonsMethod_jac(secular_equation, secular_equation_jac, u, d, startX, stepsize, tolerence, start_upper, start_lower, runs);
	//Check solutions:
}
dataA << "Roots found using newton with jacobian"<< endl << eigen << endl; 
dataA << "Completed using: " << f_calls << " function calls" << endl << endl;

dataA << endl << "Proof of rootness: The following are the function values for each eigenvector calculated:" << endl;
vec outputs(1);
for(int i = 0 ; i<n; i++){
	outputs(0) = eigen(i);
	dataA << "Eigenvalue: " << eigen_jac(i) << "\tF(eig)=" << secular_equation(u,d,outputs) << endl; 
}


dataA.close();
return 0;
}
