#include "exam.hh"
int f_calls = 0;
vec secular_equation(vec &u, vec &d, vec X){
	double x = X(0);
	f_calls++;
	int n = u.n_elem;
	vec fX(1);
	fX(0) = 1;
	for(int i = 0; i<n; i++){
		fX(0)+=u(i)*u(i)/(d(i)-x); //The equation described
	}
	return fX;
}





int main(){

ofstream dataB;
dataB.open("out.B.txt");
dataB << "Exam part B" << endl << endl;
dataB << "For this excercise i have chosen to implement what is called Brents method because it uses quite a few different methods combined into 1. " << endl;
dataB << "First of all, it implements the slow but extremely reliable bisection method to approach the root." << endl;
dataB << "It also uses a inverse quadratic linesearch instead of a linear one as i used in Newtons method.";
dataB << endl << "The last thing it uses is the secant method to get closer to a root. As you can see there are two approach methods which are used optimally together as described by Brent" << endl << endl; 

//Create the system we want to investigate:
int n = 5;
mat D(n,n,fill::zeros);
D.diag() = randu<vec>(n)*n; //Nicer numbers
vec d = D.diag();
vec u=randu<vec>(n);

dataB << "This is D" << endl << endl << D << endl << endl << "This is u" << endl << endl <<  u << endl << endl;
//In this case sigma=1 thus simplifying our problem slightly compared to the books description.
//The secular equation is inserted at the top. Now we just need to find roots within given
//Intervals as described in equations 21/22


vec d_sorted = sort(d); //Sorted version for eigenvalue search
vec search_start(n); //For the start guesses
vec search_lower(n); //For the lower limit of i'th eigenvalue
vec search_upper(n); //For the upper limit of the 'ith eigenvalue
vec eigen(n); //Container for eigenvalues
for(int i = 0; i < n-1; i++){ 
	search_lower(i) = d_sorted(i);
	search_upper(i) = d_sorted(i+1);

}
search_lower(n-1)  = d_sorted(n-1);
search_upper(n-1)  = d_sorted(n-1)+dot(u,u);

double tolerence = 1e-9, max_iter = 1e6;
f_calls=0;
double start_lower, start_upper;
vec res(1);
f_calls = 0;
for (int i = 0; i<n; i++){
	start_lower = search_lower(i);
	start_upper = search_upper(i);
	res = Brent(secular_equation, u, d, start_upper, start_lower,tolerence,max_iter);
	eigen(i)=res(0);
}
dataB << endl << endl <<"Roots found using Brents method: (combination of bisection/secant method/inverse quadratic linesearch)" << endl << eigen << endl; 
dataB << "Completed using: " << f_calls << " function calls" << endl << endl;


dataB << endl << "Proof of rootness: The following are the function values for each eigenvector calculated:" << endl;
vec outputs(1);
for(int i = 0 ; i<n; i++){
	outputs(0) = eigen(i);
	dataB << "Eigenvalue: " << eigen(i) << "\tF(eig)=" << secular_equation(u,d,outputs) << endl; 
}

dataB.close();
return 0;
}
