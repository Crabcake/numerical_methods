#include "exam.hh"
int func_calls=0;
vec secular_equation(vec &u, vec &d, vec X){
	double x = X(0);
	int n = u.n_elem;
	vec fX(1);
	fX(0) = 1;
	for(int i = 0; i<n; i++){
		func_calls++;
		fX(0)+=u(i)*u(i)/(d(i)-x); //The equation described
	}
	return fX;
}

mat secular_equation_jac(vec &u, vec &d, vec X){
	double x = X(0);
	int n = u.n_elem;
	mat J(1,1);
	J(0,0) = 0;
	for(int i = 0; i<n; i++){
		func_calls++;
		J(0,0)+=u(i)*u(i)/((d(i)-x)*(d(i)-x));
	}
	return J;
}




int main(){

ofstream dataC;
dataC.open("out.C.txt");
dataC << "Exam part C" << endl << endl;
dataC << "The amount of function call have been plotted as a function of the matrix size, n. See BigO.pdf for the plot. Note that all three methods satisfy the Big-O requirement. However it seems that the Newton methods use fewer function calls. This is not completely unexpected because the main benefit of Brents method is that it is far more reliable, always finding the root within the given interval and not outside as sometimes is possible for newtons method." << endl << endl;
dataC << endl << endl << "n\tn_n\tn_njac\tn_brent" << endl;
for(int j = 10; j<55; j++){
int n = 0.2*j*j;
mat D(n,n,fill::zeros);
D.diag() = randu<vec>(n)*n; //Nicer numbers
vec d = D.diag();
vec u=randu<vec>(n);

//In this case sigma=1 thus simplifying our problem slightly compared to the books description.
//The secular equation is inserted at the top. Now we just need to find roots within given
//Intervals as described in equations 21/22
func_calls = 0;
vec d_sorted = sort(d); //Sorted version for eigenvalue search
vec search_start(n); //For the start guesses
vec search_lower(n); //For the lower limit of i'th eigenvalue
vec search_upper(n); //For the upper limit of the 'ith eigenvalue
vec eigen(n); //Container for eigenvalues
for(int i = 0; i < n-1; i++){ 
	search_start(i) = (d_sorted(i+1)+d_sorted(i))/2.0;
	search_lower(i) = d_sorted(i);
	search_upper(i) = d_sorted(i+1);

}
search_start(n-1) = (2.0*d_sorted(n-1)+dot(u,u))/2.0;
search_lower(n-1)  = d_sorted(n-1);
search_upper(n-1)  = d_sorted(n-1)+dot(u,u);

double tolerence = 1e-9, stepsize = 1e-6;
int runs;
vec startX(n), start_lower(n), start_upper(n);
for (int i = 0; i<n; i++){
	startX = {search_start(i)};
	start_lower = {search_lower(i)};
	start_upper = {search_upper(i)};
	runs = 0;
	eigen(i) = NewtonsMethod(secular_equation, u, d, startX, stepsize, tolerence, start_upper, start_lower, runs);
}
dataC << n << "\t" << func_calls;



func_calls = 0;
vec search_start_jac(n); //For the start guesses
vec search_lower_jac(n); //For the lower limit of i'th eigenvalue
vec search_upper_jac(n); //For the upper limit of the 'ith eigenvalue
vec eigen_jac(n); //Container for eigenvalues
for(int i = 0; i < n-1; i++){ 
	search_start_jac(i) = (d_sorted(i+1)+d_sorted(i))/2.0;
	search_lower_jac(i) = d_sorted(i);
	search_upper_jac(i) = d_sorted(i+1);

}
search_start_jac(n-1)  = (2.0*d_sorted(n-1)+dot(u,u))/2.0;
search_lower_jac(n-1)  = d_sorted(n-1);
search_upper_jac(n-1)  = d_sorted(n-1)+dot(u,u);

for (int i = 0; i<n; i++){
	startX = {search_start_jac(i)};
	start_lower = {search_lower_jac(i)};
	start_upper = {search_upper_jac(i)};
	runs = 0;
	eigen_jac(i) = NewtonsMethod_jac(secular_equation, secular_equation_jac, u, d, startX, stepsize, tolerence, start_upper, start_lower, runs);
}

dataC << "\t" << func_calls;



func_calls = 0;
vec search_start_brent(n); //For the start guesses
vec search_lower_brent(n); //For the lower limit of i'th eigenvalue
vec search_upper_brent(n); //For the upper limit of the 'ith eigenvalue
vec eigen_brent(n); //Container for eigenvalues
for(int i = 0; i < n-1; i++){ 
	search_lower_brent(i) = d_sorted(i);
	search_upper_brent(i) = d_sorted(i+1);

}
search_lower_brent(n-1)  = d_sorted(n-1);
search_upper_brent(n-1)  = d_sorted(n-1)+dot(u,u);

double max_iter = 1e6;
vec res(1);
double lower, upper;
for (int i = 0; i<n; i++){
	lower = search_lower_brent(i);
	upper = search_upper_brent(i);
	res = Brent(secular_equation, u, d, upper, lower,tolerence,max_iter);
	eigen(i)=res(0);
}

dataC << "\t" << func_calls << "\n";




}




dataC.close();
return 0;
}
