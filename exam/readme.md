Exam project for Casper Muurholm

I have created three different sub-excercises based on the primary excercise given for the exam. The structure of files are: Three main files which call different functions in exam.cpp to complete each excercise. 

These Excercises are concerned with solving a symmetric rank-1 update of a size-n symmetric eigenvalue problem. The problem starts with a given diagonmatrix D, and a vector u, such that A=D+u(u^T). 

Excercise A: Solve the symmetric rank-1 update of a size-n symmetric eigenvalue problem using Newtons root finding algorithm with and without user-supplied jacobian.

Excercise B: Solve the above problem using another interesting root-finding algorithm with same Big-O (O(n^2), n is the matrix size).

Excercise C: Comment on the methods and show that the order is indeed O(n^2).



The answers to the above are supplied in the output files connected to each excercise.
