#include "integ.hh"


double adapt_integrator(function<double(double)> func, double a, double b, double acc, double eps, double f2, double f3, int calls, double &err){ //f2 nd f3 are points which are passed to make it possible to reuse these on subsequent iterations as per the chapter
	if(calls > 1e6){cerr << "Probably not going to converge... too many recursions.";}
	assert(calls < 1e6+2);
	double f1 = func(a+(b-a)*1.0/6.0);
	double f4 = func(a+(b-a)*5.0/6.0); //The points which have to be calculated due to new limits

	double Q = (2*f1+f2+f3+2*f4)*(b-a)/6.0;
	double q = (f1+f2+f3+f4)*(b-a)/4.0;
	double tol = acc+eps*fabs(Q);
	       err = fabs(Q-q);
	if(err < tol){return Q;}
	else{
		double Q1=adapt_integrator(func,a,(a+b)/2.0,acc/sqrt(2.0),eps,f1,f2,calls+1,err);
		double Q2=adapt_integrator(func,(a+b)/2.0,b,acc/sqrt(2.0),eps,f3,f4,calls+1,err); //Reusing points
		return Q1+Q2;
	}
}

double integrate(function<double(double)> func,double a,double b,double acc, double eps, double &err){
	//Calculate the middle points
	if(isinf(a) && isinf(b)){
		a=-1;
		b= 1;
		function<double(double)> f_var_sub = [func](double x){
			return func(x/(1-x*x))*(1+x*x)/((1-x*x)*(1-x*x));	
		};
		double f2 = f_var_sub(a+(b-a)*2.0/6.0);
		double f3 = f_var_sub(a+(b-a)*4.0/6.0);
	  	int calls = 0;
		double Q = adapt_integrator(f_var_sub,a,b,acc,eps,f2,f3,calls,err);
		return Q;
	}

	else if(isinf(a)){
		function<double(double)> f_var_sub = [func,b](double x){
			return func(b+x/(1+x))/((1+x)*(1+x));	
		};
		a=-1;
		b=0;
		double f2 = f_var_sub(a+(b-a)*2.0/6.0);
		double f3 = f_var_sub(a+(b-a)*4.0/6.0);
		int calls = 0;
		double Q = adapt_integrator(f_var_sub,a,b,acc,eps,f2,f3,calls,err);
		return Q;
	}

	else if(isinf(b)){
		function<double(double)> f_var_sub = [func,a](double x){
			return func(a+x/(1-x))/((1-x)*(1-x));	
		};
		a=0;
		b=1;
		double f2 = f_var_sub(a+(b-a)*2.0/6.0);
		double f3 = f_var_sub(a+(b-a)*4.0/6.0);
		int calls = 0;
		double Q = adapt_integrator(f_var_sub,a,b,acc,eps,f2,f3,calls,err);
		return Q;
	}

	else{
		double f2 = func(a+(b-a)*2.0/6.0);
		double f3 = func(a+(b-a)*4.0/6.0);
		int calls = 0;
		double Q = adapt_integrator(func,a,b,acc,eps,f2,f3,calls,err);
		return Q;
	}
}

