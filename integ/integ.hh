#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>
#include <iomanip>
#include <assert.h>
#include <gsl/gsl_integration.h>

#ifndef integ_guard
#define integ_guard

using namespace std;
using namespace arma;


double adapt_integrator(function<double(double)> func, double a, double b, double acc, double eps, double f2, double f3, int calls, double &err);

double integrate(function<double(double)> func,double a,double b,double acc, double eps, double &err);

#endif
