#include "integ.hh"

int func_calls;
double fsqrt(double x) {return sqrt(x);}
double finvroot(double x) {return 1.0/sqrt(x);}
double floginvroot(double x) {return log(x)/sqrt(x);}
double fpi(double x) {
	func_calls++;
	return 4.0*sqrt(1-(1-x)*(1-x));
	}
double fgauss(double x) {
	func_calls++;
	return exp(-x*x);
	}
double fpihalf(double x) {
	func_calls++;
	return 1.0/(1+x*x);
	}

double fgslgauss(double x, void *params){
	func_calls++;
	return exp(-x*x);
	}		
double fgslpihalf(double x, void *params){
	func_calls++;
	return 1.0/(1+x*x);
	}	

int main(){

ofstream dataAB;
dataAB.open("out.AB.txt");
double error = 0;
dataAB << endl << "Ex A" << endl;

dataAB << endl <<"Integral of sqrt(x) from 0 to 1 is: " << integrate(fsqrt,0,1,1e-9,1e-9,error) << endl << "It should be 2/3" << endl;

dataAB << endl <<"Integral of 1/sqrt(x) from 0 to 1 is: " << integrate(finvroot,0,1,1e-9,1e-9,error) << endl << "It should be 2" << endl;

dataAB << endl <<"Integral of log(x)/sqrt(x) from 0 to 1 is: " << integrate(floginvroot,0,1,1e-9,1e-9,error) << endl << "It should be -4" << endl;

func_calls = 0;
dataAB << endl <<"Integral of 4*sqrt(1-(1-x)^2) from 0 to 1 is: " << integrate(fpi,0,1,1e-9,1e-9,error) << endl << "It should be pi" << endl;
dataAB << "It used: " << func_calls << " Evaluations" << endl;
dataAB << endl << "Ex B" << endl;

func_calls = 0;
dataAB << endl <<"Integral of exp(-x^2) from -infinity to infinity is: " << integrate(fgauss,-INFINITY,INFINITY,1e-9,1e-9,error) << endl << "It should be 1.77245" << endl;


dataAB << endl << "Function calls of gaussian integral using own adaptive: " << func_calls << endl;

//Running gsl stuff as in previous course:
size_t limit = 10000;
double result;
gsl_integration_workspace *w = gsl_integration_workspace_alloc(limit);
gsl_function F;
F.function = &fgslgauss;
F.params = NULL;
func_calls = 0;
gsl_integration_qagi(&F,1e-6,1e-6,limit,w,&result,&error);
gsl_integration_workspace_free(w);
dataAB << "Function calls using gsl's QAGI: " << func_calls; 


dataAB << endl << endl << "Integral of 1/(1+x^2) from 0 to infinity is: " << integrate(fpihalf,0,INFINITY,1e-9,1e-9,error) << endl << "It should be pi half" << endl;

dataAB << endl << "Function calls of 1/(1+x^2) integral using own adaptive: " << func_calls << endl;
//Running gsl stuff as in previous course:
limit = 10000;
func_calls = 0;
gsl_integration_workspace *w1 = gsl_integration_workspace_alloc(limit);
gsl_function F1;
F1.function = &fgslpihalf;
F1.params = NULL;
gsl_integration_qagiu(&F1,0.0,1e-6,1e-6,limit,w1,&result,&error);
gsl_integration_workspace_free(w1);
dataAB << "Function calls using gsl's QAGI: " << func_calls << endl; 



dataAB.close();

return 0;
}
