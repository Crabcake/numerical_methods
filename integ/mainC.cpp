#include "integ.hh"
#include "../ode/ode.hh"

int func_calls;
double fsqrt(double x) {
	func_calls++;
	return sqrt(x);
	}
double finvroot(double x) {return 1.0/sqrt(x);}
double floginvroot(double x) {return log(x)/sqrt(x);}
double fpi(double x) {return 4.0*sqrt(1-(1-x)*(1-x));}
double fgauss(double x) {
	func_calls++;
	return exp(-x*x);
	}
double fpihalf(double x) {
	func_calls++;
	return 1.0/(1+x*x);
	}

double fgslgauss(double x, void *params){
	func_calls++;
	return exp(-x*x);
	}		
double fgslpihalf(double x, void *params){
	func_calls++;
	return 1.0/(1+x*x);
	}	

vec diffeq(double t, vec &y){
	 func_calls++;
         vec dydt(1);
         dydt(0)=sqrt(t);
	 return dydt;
	 }



int main(){

ofstream dataC;
dataC.open("out.C.txt");
double error = 0;
dataC << endl << "Ex C" << endl;



double t0_diff=0;
vec y0_diff = {0};
mat y_diff(1,1);
y_diff.col(0)=y0_diff;

vec t_diff(1);
t_diff(0) = t0_diff;

func_calls = 0;
double h=0.01, b=1, acc = 1e-6, eps = 1e-6;
driver(diffeq, t_diff, y_diff,h,b,acc,eps);

dataC << "Integral of sqrt(x) from 0 to 1 using ode is: " << y_diff(y_diff.n_elem-1) << endl << "It used " << func_calls << " evaluations";

func_calls = 0;
dataC << endl << "integral of sqrt(x) from 0 to 1 using the adaptive integrator is: " << integrate(fsqrt,0,1,1e-6,1e-6,error) << endl;
dataC << "Using: " << func_calls << " evaluations" << endl;

dataC.close();

return 0;
}
