#include "interpolation.hh"
using namespace std;


cspline::cspline(vector<double>& x_values, vector<double>& y_values) 
:n(x_values.size()), x(x_values), y(y_values), b(x_values.size()), c(x_values.size()-1), d(x_values.size()-1)  //initialization list. Now includes the precalculation of b and c factors
{
	//h[i] and p[i] for use in following loops:
	vector<double> dx(n-1), p(n-1);
	for (int i=0;i<(n-1);i++){
	dx[i]=x[i+1]-x[i];
	p[i]=(y[i+1]-y[i])/dx[i]; //remember this is simply the slope
	}
	
	//We need to calculate the described D,Q,B to get the b's from which we can calculate c,d.
	vector<double> D(n), Q(n-1), B(n); 
	//We have some start values:
	D[0]=2;
	Q[0]=1;
	B[0]=3.0*p[0];
	B[n-1]=3.0*p[n-1];
	for(int i = 0; i<n-1 ; i++){
		D[i+1]=2*dx[i]/dx[i+1]+2;
		Q[i+1]=dx[i]/dx[i+1];
		B[i+1]=3*(p[i]+p[i+1]*dx[i]/dx[i+1]);
	}
	//The tildes:
	for(int i = 0; i<n-1; i++){
		D[i+1] = D[i+1]-Q[i]/D[i];//Dtilde
		B[i+1] = B[i+1]-B[i]/D[i];
	}

	//Now calc b(i)'s, eq 27
	b[n-1] = B[n-1]/D[n-1];
	for(int i = n-2; i>=0; i--){
		b[i] = (B[i]-Q[i]*b[i+1])/D[i];
	}
	//Eq 18 gives c's and d's
	for(int i = 0; i < n-1; i++){
		c[i] = (-2.0*b[i]-b[i+1]+3.0*p[i])/dx[i];
		d[i] = (b[i]+b[i+1]-2.0*p[i])/(dx[i]*dx[i]);
	}	
	

}	



int cspline::binary_search(double z)
{
	assert(z<=x[n-1] && z>=x[0]);
	//cout << z << " " << x[0] << " " << x[n-1];
	int l = 0, r=n-1, m; //left and right, m is middle
	while( (r-l) > 1 ){ 
		m=(l+r)/2;
		if (z>x[m]) l=m;
		else r=m;
		//cout << "l=" << l <<" r=" << r << " m=" << m << "\n";
		}
	return l;
}

double cspline::evaluation(double z)
{
	int i = binary_search(z);
	double dx = z-x[i];
	return y[i]+dx*(b[i]+dx*(c[i]+dx*d[i]));
}

double cspline::integral(double z)
{
	int m = binary_search(z);
	double integ = 0, dx;
	for(int i=0;i<m;i++){ //from x0 to the interval with Z
		dx=x[i+1]-x[i];
		integ += y[i]*dx+(1.0/2.0)*b[i]*pow(dx,2)+(1.0/3.0)*c[i]*pow(dx,3)+(1.0/4.0)*d[i]*pow(dx,4); 
	}
	dx=z-x[m];
	integ += y[m]*dx+(1.0/2.0)*b[m]*pow(dx,2)+(1.0/3.0)*c[m]*pow(dx,3)+(1.0/4.0)*d[m]*pow(dx,4); 
	return integ;	
}

double cspline::derivative(double z)
{
	int m = binary_search(z);
	double deriv, dx;
	dx = z-x[m];
	deriv = b[m]+2*c[m]*dx+3.0*d[m]*dx*dx;
	return deriv;
}
