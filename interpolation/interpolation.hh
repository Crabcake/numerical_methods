#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <stdlib.h>
#include <assert.h>

#ifndef interpolation_guard
#define interpolation_guard

using namespace std;

class lspline{
	
	public:
		int n;
		vector<double> x,y;
		lspline(vector<double>& x_values, vector<double>& y_values);
		double evaluation(double z);
		double integral(double z);
		int binary_search(double z);
	private:

	protected:

};


class qspline{

	public:
		int n;
		vector<double> x,y,b,c;
		qspline(vector<double>& x_values, vector<double>& y_values);
		double evaluation(double z);
		double integral(double z);
		double derivative(double z);
		int binary_search(double z);
	private:

	protected:

};


class cspline{

	public:
		int n;
		vector<double> x,y,b,c,d;
		cspline(vector<double>& x_values, vector<double>& y_values);
		double evaluation(double z);
		double integral(double z);
		double derivative(double z);
		int binary_search(double z);
	private:

	protected:

};




#endif
