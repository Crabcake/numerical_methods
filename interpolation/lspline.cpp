#include "interpolation.hh"
using namespace std;


lspline::lspline(vector<double>& x_values, vector<double>& y_values) //think this is called the contructor. Always executed when an lspline object is created.
:n(x_values.size()), x(x_values), y(y_values)  //initialization list
{}	
//Don't think i need a body here since, we don't precalculate for lin. spline



int lspline::binary_search(double z)
{
	assert(z<=x[n-1] && z>=x[0]);
	//cout << z << " " << x[0] << " " << x[n-1];
	int l = 0, r=n-1, m; //left and right, m is middle
	while( (r-l) > 1 ){ 
		m=(l+r)/2;
		if (z>x[m]) l=m;
		else r=m;
		//cout << "l=" << l <<" r=" << r << " m=" << m << "\n";
		}
	return l;
}

double lspline::evaluation(double z)
{
	int i = binary_search(z);
	return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double lspline::integral(double z)
{
	int m = binary_search(z);
	double integ = 0, ai, am;
	for(int i=0;i<m;i++){ //from x0 to the interval with Z
		ai     = (y[i+1]-y[i])/(x[i+1]-x[i]);
		integ += y[i]*(x[i+1]-x[i]); //"box below"
		integ += (1.0/2.0) * ai * pow(x[i+1]-x[i],2); //incline 
	}
	am     = (y[m+1]-y[m])/(x[m+1]-x[m]);
	integ += y[m]*(z-x[m])+(1.0/2.0) * am * pow(z-x[m],2); //both in one
	return integ;	
}

