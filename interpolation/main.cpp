#include "interpolation.hh"
using namespace std;

int main(){

//The known spline points: Testing on consine function
int size_table = 20, spline_points = 50;
vector<double> x_table, y_table; //container with size_table elements
ofstream spline_points_stream;
spline_points_stream.open ("spline_points.txt");
for (double i=0;i<=size_table;i++) //Not -1 because i need to reach further than below
{
	double x_spline_points = 4.0*M_PI*i/size_table;
       	double y_spline_points = cos(x_spline_points);
	x_table.push_back(x_spline_points); 
	y_table.push_back(y_spline_points);
	spline_points_stream << x_spline_points << "\t" << y_spline_points << "\n";

}
spline_points_stream.close();

class lspline ls(x_table,y_table); 
ofstream lspline_stream;
lspline_stream.open ("out.lspline.txt");

for(double i=0;i<=spline_points-1;i++){
	double x_lspline = 4.0*M_PI*i/spline_points;
	lspline_stream << x_lspline << "\t"<< ls.evaluation(x_lspline) << "\t" << ls.integral(x_lspline) << "\n";
}
lspline_stream.close();



class qspline qs(x_table,y_table); 
ofstream qspline_stream;
qspline_stream.open ("out.qspline.txt");

for(double i=0;i<=spline_points-1;i++){
	double x_qspline = 4.0*M_PI*i/spline_points;
	qspline_stream << x_qspline << "\t"<< qs.evaluation(x_qspline) << "\t" << qs.integral(x_qspline) << "\t" << qs.derivative(x_qspline) << "\n";
}
qspline_stream.close();

class cspline cs(x_table,y_table); 
ofstream cspline_stream;
cspline_stream.open ("out.cspline.txt");

for(double i=0;i<=spline_points-1;i++){
	double x_cspline = 4.0*M_PI*i/spline_points;
	cspline_stream << x_cspline << "\t"<< cs.evaluation(x_cspline) << "\t" << cs.integral(x_cspline) << "\t" << cs.derivative(x_cspline) << "\n";
}
cspline_stream.close();


}
