#include "interpolation.hh"
using namespace std;


qspline::qspline(vector<double>& x_values, vector<double>& y_values) 
:n(x_values.size()), x(x_values), y(y_values), b(x_values.size()-1), c(x_values.size()-1)  //initialization list. Now includes the precalculation of b and c factors
{
	//h[i] and p[i] for use in following loops:
	vector<double> dx(n-1), p(n-1);
	for (int i=0;i<(n-1);i++){
	dx[i]=x[i+1]-x[i];
	p[i]=(y[i+1]-y[i])/dx[i]; //remember this is simply the slope
	}
	// Now the two recursions as described in the text: up first.
	for (int i=0;i<(n-2);i++){
	c[i+1]=(1.0/dx[i+1])*(p[i+1]-p[i]-c[i]*dx[i]);  //First one 0 by default of double type
	}
	// down
	c[n-2]/=2; //last value halved
	for (int i=(n-3);i>=0;i--){	
	c[i]=(1.0/dx[i])*(p[i+1]-p[i]-c[i+1]*dx[i+1]);
	}
	for (int i=0;i<(n-1);i++){
	b[i]=p[i]-c[i]*dx[i];
	}
}	



int qspline::binary_search(double z)
{
	assert(z<=x[n-1] && z>=x[0]);
	//cout << z << " " << x[0] << " " << x[n-1];
	int l = 0, r=n-1, m; //left and right, m is middle
	while( (r-l) > 1 ){ 
		m=(l+r)/2;
		if (z>x[m]) l=m;
		else r=m;
		//cout << "l=" << l <<" r=" << r << " m=" << m << "\n";
		}
	return l;
}

double qspline::evaluation(double z)
{
	int i = binary_search(z);
	return y[i]+b[i]*(z-x[i])+c[i]*pow((z-x[i]),2);
}

double qspline::integral(double z)
{
	int m = binary_search(z);
	double integ = 0, dx;
	for(int i=0;i<m;i++){ //from x0 to the interval with Z
		dx=x[i+1]-x[i];
		integ += y[i]*dx+(1.0/2.0)*b[i]*pow(dx,2)+(1.0/3.0)*c[i]*pow(dx,3); 
	}
	dx=z-x[m];
	integ += y[m]*dx+(1.0/2.0)*b[m]*pow(dx,2)+(1.0/3.0)*c[m]*pow(dx,3); 
	return integ;	
}

double qspline::derivative(double z)
{
	int m = binary_search(z);
	double deriv, dx;
	dx = z-x[m];
	deriv = b[m]+2*c[m]*dx;
	return deriv;
}
