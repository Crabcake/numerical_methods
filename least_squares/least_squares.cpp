#include "least_squares.hh"
#include "qr.hh"




vec least_squaresAB(vec &x, vec &y, vec &dy, vec &c, std::vector<std::function<double(double)>> &fitfunctions){
	
	int n = x.n_elem;
	int m = fitfunctions.size(); 	

	vec b(n);
	mat A(n,m);
	mat R(A.n_cols,A.n_cols);

	for(int i = 0; i<n; i++){
		b(i)=y(i)/dy(i);
		for (int j = 0; j<m; j++){
			A(i,j)=fitfunctions[j](x(i))/dy(i);
		}
	}
	mat AtA = A.t()*A;
	class qr_gs qr(A,R); 
	qr.solve(b,A,R,c);
	
	mat R_AtA(m,m);
	mat Sigma(m,m);
	class qr_gs qr2(AtA,R_AtA);
	qr2.inverse(Sigma,AtA,R_AtA);
	vec sigma(m);
	for (int i=0;i<m;i++){
        sigma(i)=sqrt(Sigma(i,i));	
	}
	

	return sigma;	
}

