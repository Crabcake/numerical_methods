#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>

#ifndef ls_guard
#define ls_guard

//using namespace std;
using namespace arma;

vec least_squaresAB(vec &x, vec &y, vec &dy, vec &c, std::vector<std::function<double(double)> > &fitfunctions);


#endif
