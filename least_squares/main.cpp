#include "qr.hh"
#include "least_squares.hh"


//Excercise A
int main(){
cout << "Outputs are ind pdf files. data fitted are in expdata and noisy_polynomial. Fitted data are in dataA and dataB" << endl;
ofstream exp_stream;
exp_stream.open("expdata.txt");

vector<function<double(double)> > fitfunctions;
fitfunctions.push_back([](double x){return 1.0/x;});
fitfunctions.push_back([](double x){return 1.0;});
fitfunctions.push_back([](double x){return x;});

vec x = {0.1000,  0.145,  0.211,  0.307,   0.447,   0.649,   0.944,   1.372,   1.995,   2.900};
vec y = {12.644,  9.235,  7.377,  6.460,   5.555,   5.896,   5.673,   6.964,   8.896,  11.355};
vec dy= {0.8580,  0.359,  0.505,  0.403,   0.683,   0.605,   0.856,   0.351,   1.083,   1.002};
vec c(fitfunctions.size()); 

for (int i = 0; i<x.n_elem; i++){
	exp_stream << x(i) << "\t" << y(i) << "\t" << dy(i) << "\n";
}


exp_stream.close();

vec sigma = least_squaresAB(x,y,dy,c,fitfunctions);

double data_points = 1000.0;
double delta = (x(x.n_elem-1)-x(0))/data_points;




ofstream dataA;
dataA.open("dataA.txt");
for (double i = x(0);i<=x(x.n_elem-1);i+=delta){
	double y_fit     = (c(0)*fitfunctions[0](i)+c(1)*fitfunctions[1](i)+c(2)*fitfunctions[2](i));
	double y_fit_low = ((c(0)-sigma(0))*fitfunctions[0](i)+(c(1)-sigma(1))*fitfunctions[1](i)+(c(2)-sigma(2))*fitfunctions[2](i));
	double y_fit_high= ((c(0)+sigma(0))*fitfunctions[0](i)+(c(1)+sigma(1))*fitfunctions[1](i)+(c(2)+sigma(2))*fitfunctions[2](i));
	dataA << i << "\t" << y_fit << "\t" << y_fit_low << "\t" << y_fit_high << "\n";
}
dataA.close();




//Excercise B

//First we generate some data
ofstream np_stream;
np_stream.open("noisy_polynomial.txt");

vector<function<double(double)> > fitfunctions_poly;
fitfunctions_poly.push_back([](double x){return 1.0;});
fitfunctions_poly.push_back([](double x){return x;});
fitfunctions_poly.push_back([](double x){return x*x;});

unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
std::default_random_engine generator (seed);


int length = 6;
double start=2, end = 10, points = 6;
double change_poly = (end-start)/points;
double a_poly=2.0, b_poly=0.0, constant_poly=1.0;
vec x_poly(length), y_poly(length), dy_poly(length);
vec c_poly(fitfunctions.size());
for (double i = 0; i<length; i++){
x_poly(i) = start+change_poly*i;
normal_distribution<double> distribution1 (3.0,5);
dy_poly(i)= distribution1(generator);
normal_distribution<double> distribution2 (0.0,3.0);
y_poly(i) = a_poly*x_poly(i)*x_poly(i)+b_poly*x_poly(i)+constant_poly+distribution2(generator);
np_stream << x_poly(i) << "\t" << y_poly(i) << "\t" << dy_poly(i) << endl;
}
np_stream.close();


vec sigmaB = least_squaresAB(x_poly,y_poly,dy_poly,c_poly,fitfunctions_poly);
ofstream dataB;
dataB.open("dataB.txt");
double delta_poly = (x_poly(x_poly.n_elem-1)-x_poly(0))/data_points;

for (double i = x_poly(0);i<=x_poly(x_poly.n_elem-1);i+=delta_poly){
	double y_fit_poly     = (c_poly(0)*fitfunctions_poly[0](i)+c_poly(1)*fitfunctions_poly[1](i)+c_poly(2)*fitfunctions_poly[2](i));
	double y_fit_poly_low = ((c_poly(0)-sigmaB(0))*fitfunctions_poly[0](i)+(c_poly(1)-sigmaB(1))*fitfunctions_poly[1](i)    +(c_poly(2)-sigmaB(2))*fitfunctions_poly[2](i));
	double y_fit_poly_high= ((c_poly(0)+sigmaB(0))*fitfunctions_poly[0](i)+(c_poly(1)+sigmaB(1))*fitfunctions_poly[1](i)    +(c_poly(2)+sigmaB(2))*fitfunctions_poly[2](i));
	dataB << i << "\t" << y_fit_poly << "\t" << y_fit_poly_low << "\t" << y_fit_poly_high << "\n";
}
dataB.close();





return 0;
}
