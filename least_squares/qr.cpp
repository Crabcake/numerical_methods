#include "qr.hh"
#include <armadillo>
#include <iostream>
#include <assert.h>

using namespace std;
using namespace arma;


qr_gs::qr_gs(Mat<double>& A, Mat<double>& R) 
:n(A.n_rows), m(A.n_cols)
{
for (int i = 0; i < m; i++){
	for (int j = 0; j<n; j++){
		R(i,i)+=(A(j,i)*A(j,i));
	}
	R(i,i)=pow(R(i,i),0.5);
	for (int j = 0; j<n; j++){
		A(j,i)=A(j,i)/R(i,i);
	}
	for (int j = i+1; j < m ; j++){
	
		for (int k = 0; k<n; k++){
			R(i,j)+=A(k,i)*A(k,j);
		}
		for (int k = 0; k<n ; k++){
			A(k,j)=A(k,j)-A(k,i)*R(i,j);
		}
	}
}
}

void qr_gs::solve(Col<double>& b, Mat<double> Q, Mat<double> R,Col<double>& x) //This is probably not optimal but i am having trouble accessing A and R from the contructor. So hotfix....
{
int m=R.n_rows;
vec c = Q.t()*b;
for (int i=(m-1);i>=0;i--){

	x(i) = c(i);
	for (int k=i+1;k<m;k++){
		x(i)-=R(i,k)*x(k);
	}
	x(i) /= R(i,i);
}
}

void qr_gs::inverse(Mat<double>& B,Mat<double> Q, Mat<double> R)
{
B = B.eye(); //Setting B to start as identity matrix
for (int i = 0;i<n;i++)
{
	vec b_i=B.col(i);  
	solve(b_i,Q,R,b_i);  //Check this. I modified the above backsub but that changed this one too. It might not function as it should..
	//cout << "\nThis should be unit vector\n" << Q*R*b_i;
	B.col(i) = b_i;
}
//cout << Q*R*B;
}
