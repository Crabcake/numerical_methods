#include "linear_equations.hh"

qr_gs::qr_gs(Mat<double>& A, Mat<double>& R) 
:n(A.n_rows), m(A.n_cols)
{
for (int i = 0; i < m; i++){
	for (int j = 0; j<n; j++){
		R(i,i)+=(A(j,i)*A(j,i));
	}
	R(i,i)=pow(R(i,i),0.5);
	for (int j = 0; j<n; j++){
		A(j,i)=A(j,i)/R(i,i);
	}
	for (int j = i+1; j < m ; j++){
	
		for (int k = 0; k<n; k++){
			R(i,j)+=A(k,i)*A(k,j);
		}
		for (int k = 0; k<n ; k++){
			A(k,j)=A(k,j)-A(k,i)*R(i,j);
		}
	}
}
}

void qr_gs::solve(Col<double>& b, Mat<double> Q, Mat<double> R) //This is probably not optimal but i am having trouble accessing A and R from the contructor. So hotfix....
{
b = Q.t()*b;
//Rx=b to be solved
for (int i=(n-1);i>=0;i--){
	double b_i = b(i);
	for (int k=i+1;k<n;k++){
		b_i-=R(i,k)*b(k);
	}
	b(i)=b_i/R(i,i);
}
}

void qr_gs::inverse(Mat<double>& B,Mat<double> Q, Mat<double> R)
{
B = B.eye(); //Setting B to start as identity matrix
for (int i = 0;i<n;i++)
{
	vec b_i=B.col(i);  
	solve(b_i,Q,R);
	B.col(i) = b_i;
}
}

void qr_givens(mat &A){
	int r=A.n_rows;
	int c=A.n_cols;
	
	for(int q = 0; q<c; q++){
		for(int p = q+1; p<r; p++){
			double angle = atan2(A(p,q),A(q,q));
			for(int k = q; k<c; k++){
				double xq = A(q,k), xp=A(p,k);
				A(q,k) = xq*cos(angle)+xp*sin(angle);
				A(p,k) = -xq*sin(angle)+xp*cos(angle);
			}
			A(p,q) = angle;
		}
	}

	}


void qr_givens_QTvec(mat &QR, vec &v){
	int r = QR.n_rows;
	int c = QR.n_cols;

	for(int q=0; q<c; q++){
		for(int p = q+1;p<r;p++){
			double angle = QR(p,q); //Where we hid them previously...
			double vq=v(q), vp=v(p);
			v(q)=vq*cos(angle)+vp*sin(angle);
			v(p)=-vq*sin(angle)+vp*cos(angle);
		}
	}
}


void qr_givens_solve(mat &A, vec &v){
	qr_givens(A);
	qr_givens_QTvec(A,v);
	int r = A.n_rows;
	v(r-1)=v(r-1)/A(r-1,r-1);
	//BS
	for (int i = r-2; i>=0; i--){
	double sum = 0;
	for (int k = i+1;k<r;k++){
		sum+=A(i,k)*v(k);
	}
	v(i) = (v(i)-sum)/A(i,i);
	}
	
}
