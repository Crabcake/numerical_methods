#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <stdlib.h>
#include <assert.h>
#include <armadillo>

#ifndef interpolation_guard
#define interpolation_guard

using namespace std;
using namespace arma;

class qr_gs{
	
	public:
		int n,m;
		qr_gs(Mat<double>& A, Mat<double>& R);
		void solve(Col<double>& b,Mat<double> Q, Mat<double> R);
		void inverse(Mat<double>& B,Mat<double> Q, Mat<double> R); 
	private:

	protected:

};


void qr_givens(mat &A);

void qr_givens_QTvec(mat &QR, vec &v);

void qr_givens_solve(mat &A, vec &v);

#endif
