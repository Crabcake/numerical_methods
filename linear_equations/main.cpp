#include "linear_equations.hh"

int main(){

int rows = 5, columns = 4;

Mat<double> A=randn(rows,columns), R=zeros(columns,columns);

ofstream qr_stream;
qr_stream.open("out.lin_eq.txt");
qr_stream << "\n The matrix before decomposition:\n" << A << "\n";

class qr_gs qr(A,R); 
qr_stream << "After decomp. The following is R:\n" << R << "It should be upper triangular\n\n";
qr_stream << "The following is Q^tQ:\n" << A.t()*A << "It should be unit matrix\n\n";
qr_stream << "The following is QR:\n" << A*R << "It should be equal to A\n\n";


Mat<double> A2=randn(rows,rows), R2=zeros(rows,rows);
Col<double> b2=randn(rows);
Mat<double> A2Copy = A2;
Mat<double> A4 = A2;
Col<double> b4 = b2; 
qr_stream << "\n\nThe Next part of A:\nThis i b:\n" << b2 << "\n";

class qr_gs qr2(A2,R2); 
qr2.solve(b2,A2,R2);
qr_stream << "This is x:\n" << b2 << "\n";
qr_stream << "This is Ax\n" << A2*R2*b2 << "\n";


Mat<double> A3=randn(rows,rows), R3=zeros(rows,rows), B3=zeros(rows,rows);
class qr_gs qr3(A3,R3);
qr3.inverse(B3,A3,R3);
qr_stream << "\n\nPart B:\nThis is A:\n"<<A3*R3<<"\nThis is A^-1\n"<<B3<<"\nThis is the product:\n"<<A3*R3*B3;


qr_givens_solve(A4,b4);
qr_stream << "\n\nPart C: This is the solution using givens rotations: "<< endl << b4 << endl << "This is the same solution as in the second part of part A! This means that it works..." << endl;



qr_stream.close();
return 0;
}
