#include "jacobi.hh"

int jacobi_B(mat& A,vec& e,mat& V, int k){
int changed, no_rotations=0, n = A.n_rows;
for(int i=0;i<n;i++){e(i)=A(i,i);} //Gathering diagonal 
V = V.eye(n,n);

for (int p = 0;p<k;p++){
	changed = 0;
do{
	changed = 0;
//Want to zero row by row:
//The following simply iterates the upper triangle.  
	for (int q=p+1;q<n;q++){
	//calc phi using algorithm in pdf:
	double Apq=A(p,q), Aqq=e(q),App=e(p);
	double phi =0.5*atan2(2.0*Apq,Aqq-App);
	double c = cos(phi), s = sin(phi);
	//Calculate the two diagonal elements
	double App_new=c*c*App-2*s*c*Apq+s*s*Aqq;
	double Aqq_new=s*s*App+2*s*c*Apq+c*c*Aqq;
	//Now we check if new equals old using AND logic. If they do we stop and assume it is true for all the others too.
	if(App_new != App || Aqq_new != Aqq){
		no_rotations++;
		changed = 1; //so we can keep make another sweep...
		e(p) = App_new;
		e(q) = Aqq_new;
		A(p,q) = 0.0; //Because phi was designed as such...
		//The following completes the rest of the changes in 3 loops. To figure them out it is best to write down a sample matrix and see what it changes... 	
		//for(int i=0;i<p;i++){
		//	double Aip=A(i,p);
		//	double Aiq=A(i,q);
		//	A(i,p)=c*Aip-s*Aiq;
		//	A(i,q)=c*Aiq+s*Aip;
		//}
		for(int i=p+1;i<q;i++){
			double Api=A(p,i);
			double Aiq=A(i,q);
			A(p,i)=c*Api-s*Aiq;
			A(i,q)=c*Aiq+s*Api;
		}
		for(int i=q+1;i<n;i++){
			double Api=A(p,i);
			double Aqi=A(q,i);
			A(p,i)=c*Api-s*Aqi;
			A(q,i)=c*Aqi+s*Api;
		}
		for (int i=0;i<n;i++){
			double Vip = V(i,p);
			double Viq = V(i,q);
			V(i,p) = c*Vip - s*Viq;
			V(i,q) = s*Vip + c*Viq;
		}
		}
	}

}
while(changed);
}
return no_rotations;
}

int jacobi_A(mat& A,vec& e,mat& V){
int changed, no_rotations=0, n = A.n_rows;
for(int i=0;i<n;i++){e(i)=A(i,i);} //Gathering diagonal 
V = V.eye(n,n);
do{
	changed = 0;
//Want to zero row by row:
//The following simply iterates the upper triangle.  
for (int p = 0;p<n;p++){
	for (int q=p+1;q<n;q++){
	//calc phi using algorithm in pdf:
	double Apq=A(p,q), Aqq=e(q),App=e(p);
	double phi =0.5*atan2(2.0*Apq,Aqq-App);
	double c = cos(phi), s = sin(phi);
	//Calculate the two diagonal elements
	double App_new=c*c*App-2*s*c*Apq+s*s*Aqq;
	double Aqq_new=s*s*App+2*s*c*Apq+c*c*Aqq;
	//Now we check if new equals old using AND logic. If they do we stop and assume it is true for all the others too.
	if(App_new != App || Aqq_new != Aqq){
		no_rotations++;
		changed = 1; //so we can keep make another sweep...
		e(p) = App_new;
		e(q) = Aqq_new;
		A(p,q) = 0.0; //Because phi was designed as such...
		//The following completes the rest of the changes in 3 loops. To figure them out it is best to write down a sample matrix and see what it changes... 	
		for(int i=0;i<p;i++){
			double Aip=A(i,p);
			double Aiq=A(i,q);
			A(i,p)=c*Aip-s*Aiq;
			A(i,q)=c*Aiq+s*Aip;
		}
		for(int i=p+1;i<q;i++){
			double Api=A(p,i);
			double Aiq=A(i,q);
			A(p,i)=c*Api-s*Aiq;
			A(i,q)=c*Aiq+s*Api;
		}
		for(int i=q+1;i<n;i++){
			double Api=A(p,i);
			double Aqi=A(q,i);
			A(p,i)=c*Api-s*Aqi;
			A(q,i)=c*Aqi+s*Api;
		}
		for (int i=0;i<n;i++){
			double Vip = V(i,p);
			double Viq = V(i,q);
			V(i,p) = c*Vip - s*Viq;
			V(i,q) = s*Vip + c*Viq;
		}
		}
	}

}
}
while(changed);
return no_rotations;
}




