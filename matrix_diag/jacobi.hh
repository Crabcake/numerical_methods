#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <stdlib.h>
#include <assert.h>
#include <armadillo>

#ifndef interpolation_guard
#define interpolation_guard

using namespace std;
using namespace arma;

int jacobi_A(mat& A, vec& e, mat& V);
int jacobi_B(mat& A, vec& e, mat& V,int k);
#endif
