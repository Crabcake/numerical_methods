#include "jacobi.hh"

int main(){
int n=5,m=5;
ofstream jc;
jc.open("out.jacobi.txt");

// Part A
mat A=symmatu(randu<mat>(n,m));
mat Acopy = A;
mat V=zeros(n,m);
vec e=zeros(n);
//jc << "\nJacobi excercise A:\n"<<"This is A:\n" << A;
//Remember that upper triangle of A is destroyed in the function:
int A_no_rotations = jacobi_A(A,e,V);
//jc << "\nChanged A\n" << A << "\neigenvalues\n" << e << "\neigenvectors\n" << V << "\n";

jc << "\nThis is V^T*A*V\n" << V.t()*Acopy*V << "\nIt should be diagonal. (Atleast if one ignores values less than something like 10^-10)\n";
jc << "\nThe Number of rotations used is\n" << A_no_rotations;


// Part B
//mat B=symmatu(randu<mat>(n,m));
mat B = Acopy; //To check if results are the same
mat VB=zeros(n,m);
mat B2 = B, VB2=VB;
vec eB=zeros(n);
vec eB2=eB;
jc << "\n\nJacobi excercise B:\n";
//Remember that upper triangle of A is destroyed in the function:
int B_no_rotations = jacobi_B(B,eB,VB,6);
jc << "\neigenvalues\n" << eB << "\neigenvectors\n" << VB << "\n";
jc << "\nThe number of rotations used for full diag. is\n" << B_no_rotations;


int B_no_rotations_single_value = jacobi_B(B2,eB2,VB2,1);
jc << "\n\nThe number of rotations for finding the lowest eigenvalue is:\n" << B_no_rotations_single_value << "\n\nThe eigenvalue:\n" << eB2(0) << "\n";



jc << "\n\nQuestions:\n";
jc << "The elements in the first row have all been zeroed, when starting on the next row, the algorithm which changes values in the first row only depends on constants times values from the same row which are all zero. Therefore these values will not change\n\n";
jc << "\nLowest eigenvalue argument:\nIt has to do with the implementation of atan2. The following is in the cppreference:  If y is ±0 and x is negative or -0, ±π is returned. If y is ±0 and x is positive or +0, ±0 is returned. Here y is the numerator and x is the denominator in atan2. Consider two diagonal elements which have converged to an eigenvalue. If the second value is lower than the first. atan2 will have a negative denominator during the algorithm while the numerator has been zeroed han thus giving phi=0.5pi. This makes A'pp=Aqq and A'qq=App because cos(0.5pi)=0. Thus making the lowest value appear at the top";
jc << "\n\nHighest eigenvalue change:\nThe following is equivalent atan(y/x)=-atan(y/(-x)) but atan2 will not be equivalent due the above description and thus we should arrive at the highest eigenvalue first.\n";


return 0;
}
