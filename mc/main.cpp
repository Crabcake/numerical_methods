#include "mc.hh"

double f1(vec x){
	return 1.0/sqrt(x(0)*x(1)); //result from 0 to 1 and 0 to 1 is 4
}

double f2(vec x){
	return exp(-1.0*x(0)*x(1)); //Same limits should give 0.7966
}

double f3(vec x){
	return 1.0/(1-cos(x(0))*cos(x(1))*cos(x(2)))/(pow(M_PI,3));
}

int main(){

ofstream data;
data.open("out.txt");
data << "Start with Excercise A:\n";
//First "easy" functions
vec a = {0,0};
vec b = {1,1};
double err=0;
double result=0;

result = plainmc(f1,a,b,1e7,err);
data << endl << "Result for 1/sqrt(x*y): " << result << endl << "Error: " << err << endl << "Should be 4" << endl;

result = plainmc(f2,a,b,1e7,err);
data << endl << "Result for exp(-x*y): " << result << endl << "Error: " << err << endl << "Should be 0.7966" << endl;


err = 0;
vec a3 = {0,0,0};
vec b3 = {M_PI,M_PI,M_PI};
result = plainmc(f3,a3,b3,1e7,err);
data << endl << "Result for diff singular integral: " << result << endl << "Error: " << err << endl << "Should be 1.393204" << endl;

data << "\nNow for Ex B. See the plot for results...\n";
int size = 100;
double start= 30;
vec errvec(size);
vec nvec(size);

ofstream sqr_data;
sqr_data.open("sqr_data.txt");
for(double i = 0; i<size; i++){
	nvec(i) = (i+start)*(i+start)*(i+start); //better spacing
	plainmc(f2,a3,b3,nvec(i),errvec(i));
	sqr_data << nvec(i) << "\t" << errvec(i) << "\n";	
}


sqr_data.close();
data.close();

return 0;
}
