#include "mc.hh"
#define RND ((double) rand()/RAND_MAX)

double plainmc(double f(vec x), vec a, vec b, int N, double &err){
	int dim = a.n_elem;

	double V = 1;
	for(int i=0;i<dim;i++){
		V*=b(i)-a(i);  
	}
	double sum=0;
	double sum_squared = 0;
	double fx;
	for(int i=0;i<N;i++){
	fx= f(rand_values(a,b));
	sum+=fx;
	sum_squared+=fx*fx;
	}
	double avg = sum/N;
	double var = sum_squared/N-avg*avg;
	double result = avg*V;
	err = sqrt(var/N)*V;
	return result;

}

vec rand_values(vec a, vec b){ //generate random value between the limits
	int dim = a.n_elem;
	vec randoms(dim);
	for(int i=0;i<dim;i++){
	randoms(i) = a(i)+(b(i)-a(i))*RND; 
	}
	return randoms;
}
