#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>
#include <iomanip>
#include <assert.h>

#ifndef mc_guard
#define mc_guard

using namespace std;
using namespace arma;



double plainmc(double f(vec x), vec a, vec b, int N, double &err);

vec rand_values(vec a, vec b); //generate random value between the limits

#endif
