Start with Excercise A:

Result for 1/sqrt(x*y): 3.99433
Error: 0.00392669
Should be 4

Result for exp(-x*y): 0.796568
Error: 5.00586e-05
Should be 0.7966

Result for diff singular integral: 1.39245
Error: 0.00406745
Should be 1.393204

Now for Ex B. See the plot for results...
