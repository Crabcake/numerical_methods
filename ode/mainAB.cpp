#include "ode.hh"

vec harmonic_motion(double t, vec &y){
	vec dydt(2);
	dydt(0)=y(1);
	dydt(1)=-y(0);
	return dydt;
}

vec orbit(double t, vec &y){
	vec dydt(2);
	dydt(0) = y(1);
	dydt(1) = 1.0 - y(0)+0.03*y(0)*y(0); //From intro course
	return dydt;
}

//Excercise AB
int main(){

ofstream dataAB;
dataAB.open("out.AB.txt");
dataAB << "Ex AB: See the datafile for the resulting data of both diff equations or the plot." << endl << endl;

double t0_harmonic=0;
vec y0_harmonic = {0,1};

mat y_harmonic(2,1);
y_harmonic.col(0)=y0_harmonic;

vec t_harmonic(1);
t_harmonic(0) = t0_harmonic;

double h=0.01, b=9.9, acc = 1e-4, eps = 1e-4;

driver(harmonic_motion, t_harmonic, y_harmonic,h,b,acc,eps);

double t0_orbit=0;
vec y0_orbit = {1,-0.5};

mat y_orbit(2,1);
y_orbit.col(0)=y0_orbit;

vec t_orbit(1);
t_orbit(0) = t0_orbit;

h=0.01, b=9.9, acc = 1e-4, eps = 1e-4;

driver(orbit, t_orbit, y_orbit,h,b,acc,eps);

size_t i=0;
ofstream dataABdat;
dataABdat.open("diffeq_data.txt");
dataABdat << setprecision(3);

while(i<t_orbit.n_elem || i<t_harmonic.n_elem){
	if(i<t_orbit.n_elem && i<t_harmonic.n_elem){
		//When both vectors have values: (1/y because this is the radius of the orbit which i am interested in plotting...)
		dataABdat << t_harmonic(i) << "\t" << y_harmonic(0,i) << "\t" << t_orbit(i) << "\t" << 1.0/y_orbit(0,i) << endl;	
	}
	else if(i<t_harmonic.n_elem){
		dataABdat << t_harmonic(i) << "\t" << y_harmonic(0,i) << endl;
	}
	else if(i<t_orbit.n_elem){
	dataABdat << "\t\t" << t_orbit(i) << "\t" << 1.0/y_orbit(0,i) << endl; 
	}
	i++; 
}

dataAB.close();
dataABdat.close();

return 0;
}
