#include "ode.hh"

int calls;
vec harmonic_motion(double t, vec &y){
	calls++;
	vec dydt(2);
	dydt(0)=y(1);
	dydt(1)=-y(0);
	return dydt;
}

vec orbit(double t, vec &y){
	calls++;
	vec dydt(2);
	dydt(0) = y(1);
	dydt(1) = 1.0 - y(0)+0.03*y(0)*y(0); //From intro course
	return dydt;
}

//Excercise C
int main(){

ofstream dataC;
dataC.open("out.C.txt");
dataC << "Ex C:" << endl << endl;

double t0_harmonic=0;
vec y0_harmonic = {0,1};

mat y_harmonic(2,1);
y_harmonic.col(0)=y0_harmonic;

vec t_harmonic(1);
t_harmonic(0) = t0_harmonic;

double h=0.01, b=9.9, acc = 1e-6, eps = 1e-6;

calls = 0;
driver(harmonic_motion, t_harmonic, y_harmonic,h,b,acc,eps);
dataC << endl << "Harmonic motion" << endl << "The result using the original stepper is: " << y_harmonic(0,t_harmonic.n_elem-1) << endl << "Calls=" << calls;
t0_harmonic=0;
y0_harmonic = {0,1};

mat y_harmonic2(2,1);
y_harmonic2.col(0)=y0_harmonic;

vec t_harmonic2(1);
t_harmonic2(0) = t0_harmonic;

h=0.01, b=9.9, acc = 1e-6, eps = 1e-6;
calls = 0;
driver_twostep(harmonic_motion, t_harmonic2, y_harmonic2,h,b,acc,eps);
dataC << endl << endl << "The result using two-step method is: " << y_harmonic2(0,t_harmonic2.n_elem-1) << endl << "Calls=" << calls;



double t0_orbit=0;
vec y0_orbit = {1,-0.5};
mat y_orbit(2,1);
y_orbit.col(0)=y0_orbit;
vec t_orbit(1);
t_orbit(0) = t0_orbit;
calls = 0;
h=0.01, b=9.9, acc = 1e-6, eps = 1e-6;
driver(orbit, t_orbit, y_orbit,h,b,acc,eps);
dataC << endl << endl << endl << "Same for the orbit:" << endl << "The result using the orginal method is: " << y_orbit(0,t_orbit.n_elem-1) << endl << "Calls=" << calls;


t0_orbit=0;
y0_orbit = {1,-0.5};
mat y_orbit2(2,1);
y_orbit2.col(0)=y0_orbit;
vec t_orbit2(1);
t_orbit2(0) = t0_orbit;
calls = 0;
h=0.01, b=9.9, acc = 1e-6, eps = 1e-6;
driver_twostep(orbit, t_orbit2, y_orbit2,h,b,acc,eps);
dataC <<  endl << endl << "The result using the two-step method is: " << y_orbit2(0,t_orbit2.n_elem-1) << endl << "Calls=" << calls << endl << endl << "In both cases, the two step method is far superior  with respect to function calls" << endl << endl;

dataC.close();

return 0;
}
