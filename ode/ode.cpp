#include "ode.hh"


void rkstep12(vec f(double t, vec &y), double t, vec &y, double h, vec &yh, vec &err){
	int n=y.n_rows;
	vec k0(n), k1(n); //For RK12 these are all we need...
	k0 = f(t,y);
	double t_k1 = t+h;
	vec y_k1 = y+h*k0;
	k1 = f(t_k1,y_k1);  //Should match the RK12 butchers table in the text
		
	yh = y+h*0.5*(k0+k1); //Evolved
	err = (k0-k1)*h/2.0;
}

void rkstep2_twostep(vec f(double t, vec &y),double t, vec &y, double h, vec &yh, vec &err){
	int n=y.n_rows;  //Here we just call the above for one full step and two subsequent half steps.
	vec y_full_step(n), y_half_step(n);
	
	rkstep12(f, t, y, h, y_full_step, err); //Full
	rkstep12(f, t, y, h/2.0, y_half_step, err); //first halfstep
	rkstep12(f, t+h/2.0, y_half_step, h/2.0, yh, err); //Continue from above with another half. Note also that the half step is used as the solution as this is most often the most precise solution
	err = (yh-y_full_step)/(2*2-1); //Eq 11 and runges principle
	

}


void driver(vec f(double t, vec &y), vec& t, mat &y, double h, double b, double acc, double eps){
	//Done like section 1.6
	double power=0.25, safety=0.95;
	double tolerence, error; //These are used to check if the step is okay. 
	//Notes: We evolve from a to b. The initial value of t is a. The final value is b. I want ys to contain all the saved y values along the path and t the points for each y vector. 
	double a = t(0);
	int    n = y.n_rows;
     	vec yh(n), dy(n); //For the step
	while(t(t.n_elem-1)<b){ //Calculating until we reach b...
		if(t(t.n_elem-1)+h > b){h=b-t(t.n_elem-1);}//We always want to hit b though...}
		int index = t.n_elem-1;
		double t0 = t(index);
		vec y0    = y.col(index);
		//Attempt evolve:
		rkstep12(f,t0,y0,h,yh,dy);
		error = norm(dy);
		tolerence = (acc+norm(yh)*eps)*sqrt(h/(b-a));
		if(error<tolerence){	
			t.resize(index+2);
			t(index+1)=t0+h;
			y.insert_cols(index+1,yh);
			index++;
		}
		if(error>0.0){
			h *= pow(tolerence/error,power)*safety;
		}		
		else{
			h=2*h;
		}
	}
}

void driver_twostep(vec f(double t, vec &y), vec& t, mat &y, double h, double b, double acc, double eps){
	//Done like section 1.6
	double power=0.25, safety=0.95;
	double tolerence, error; //These are used to check if the step is okay. 
	//Notes: We evolve from a to b. The initial value of t is a. The final value is b. I want ys to contain all the saved y values along the path and t the points for each y vector. 
	double a = t(0);
	int    n = y.n_rows;
     	vec yh(n), dy(n); //For the step
	while(t(t.n_elem-1)<b){ //Calculating until we reach b...
		if(t(t.n_elem-1)+h > b){h=b-t(t.n_elem-1);}//We always want to hit b though...}
		int index = t.n_elem-1;
		double t0 = t(index);
		vec y0    = y.col(index);
		//Attempt evolve:
		rkstep2_twostep(f,t0,y0,h,yh,dy);
		error = norm(dy);
		tolerence = (acc+norm(yh)*eps)*sqrt(h/(b-a));
		if(error<tolerence){	
			t.resize(index+2);
			t(index+1)=t0+h;
			y.insert_cols(index+1,yh);
			index++;
		}
		if(error>0.0){
			h *= pow(tolerence/error,power)*safety;
		}		
		else{
			h=2*h;
		}
	}
}
