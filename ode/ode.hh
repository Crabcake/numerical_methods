#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>
#include <iomanip>
#ifndef ode_guard
#define ode_guard

using namespace std;
using namespace arma;

void rkstep12(vec f(double t, vec &y), double t, vec &y, double h, vec &yh, vec &err);

void driver(vec f(double t, vec &y), vec& t, mat &y, double h, double b, double acc, double eps);
void rkstep2_twostep(vec f(double t, vec &y),double t, vec &y, double h, vec &yh, vec &err);
void driver_twostep(vec f(double t, vec &y), vec& t, mat &y, double h, double b, double acc, double eps);

#endif
