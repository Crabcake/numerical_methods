#include "qr.hh"
#include "optimization.hh"

//Functions used. I have both A and B in this file. 
double functionRB(vec X){
	double x = X(0);
	double y = X(1);
	double fX=(1-x)*(1-x)+100*(y-x*x)*(y-x*x);
	return fX;
};

vec functionRBgrad(vec X){
	double x= X(0);
	double y= X(1);
	vec grad(2);
	grad(0) = 2*(200*x*x*x -200*x*y+x-1);
	grad(1) = 200*(y-x*x);
	return grad;
};


mat functionRBhess(vec X){
	double x = X(0);
	double y = X(1);
	mat H(2,2);
	H(0,0) = 2*(600*x*x-200*y+1);
	H(0,1) = -400*x;
	H(1,1) = 200;
	H(1,0) = -400*x;
	return H;
};


double functionHB(vec X){
	double x= X(0);
	double y= X(1);
	double fX=(x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);	
	return fX;
};

vec functionHBgrad(vec X){
	double x = X(0);
	double y = X(1);
	vec fX(2);
	fX(0) = 2.0*(2.0*x*(x*x+y-11)+x+y*y-7);// df/dx
	fX(1) = 2.0*(x*x+2*y*(x+y*y-7)+y-11); // df/dy
	return fX;
};

mat functionHBhess(vec X){
	double x = X(0);
	double y = X(1);
	mat J(2,2);
	J(0,0) = 12*x*x+4*y-42;
	J(0,1) = 4*x+4*y;
	J(1,1) = 4*x+12*y*y-26;
	J(1,0) = 4*x+4*y;
	return J;
};



//Excercise A
int main(){

double tolerence=1e-3;
int iterations;
vec startX(2);
ofstream dataA;
dataA.open("out.A.txt");
dataA << "The following are the minima of the rosenbrock and himmelblau function found using newtons minimization method:" << endl << endl;

dataA << "Rosenbrock" << endl;
startX={50,-50};
iterations = NewtonsMinimization(functionRB,functionRBgrad,functionRBhess,startX,tolerence);
dataA << startX << endl << "iterations= " << iterations << endl << endl;

dataA << "Himmelblau" << endl;
startX={50,-50};
iterations = NewtonsMinimization(functionHB,functionHBgrad,functionHBhess,startX,tolerence);
dataA << startX << endl << "iterations= " << iterations << endl << endl;





dataA.close();


return 0;
}
