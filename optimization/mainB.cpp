#include "qr.hh"
#include "optimization.hh"

//Functions used. I have both A and B in this file. 

//Exp decay data
vec t = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
vec y = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
vec e = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = t.size();

//functions
double exp_decay(vec X){
	double sum = 0;
	double A = X(0);
	double T = X(1);
	double B = X(2);
	for(int i = 0; i<N; i++){
		double fx=A*exp(-t(i)/T)+B;
		       sum+=(fx-y(i))*(fx-y(i))/(e(i)*e(i));
	}
	return sum;
};



double functionRB(vec X){
	double x = X(0);
	double y = X(1);
	double fX=(1-x)*(1-x)+100*(y-x*x)*(y-x*x);
	return fX;
};

vec functionRBgrad(vec X){
	double x= X(0);
	double y= X(1);
	vec grad(2);
	grad(0) = 2*(200*x*x*x -200*x*y+x-1);
	grad(1) = 200*(y-x*x);
	return grad;
};


mat functionRBhess(vec X){
	double x = X(0);
	double y = X(1);
	mat H(2,2);
	H(0,0) = 2*(600*x*x-200*y+1);
	H(0,1) = -400*x;
	H(1,1) = 200;
	H(1,0) = -400*x;
	return H;
};


double functionHB(vec X){
	double x= X(0);
	double y= X(1);
	double fX=(x*x+y-11)*(x*x+y-11)+(x+y*y-7)*(x+y*y-7);	
	return fX;
};

vec functionHBgrad(vec X){
	double x = X(0);
	double y = X(1);
	vec fX(2);
	fX(0) = 2.0*(2.0*x*(x*x+y-11)+x+y*y-7);// df/dx
	fX(1) = 2.0*(x*x+2*y*(x+y*y-7)+y-11); // df/dy
	return fX;
};

mat functionHBhess(vec X){
	double x = X(0);
	double y = X(1);
	mat J(2,2);
	J(0,0) = 12*x*x+4*y-42;
	J(0,1) = 4*x+4*y;
	J(1,1) = 4*x+12*y*y-26;
	J(1,0) = 4*x+4*y;
	return J;
};



//Excercise B
int main(){

double tolerence=1e-6;
int iterations;
vec startX(2);
ofstream dataB;
dataB.open("out.B.txt");
dataB << "The following are the minima of the rosenbrock and himmelblau function found using newtons minimization method with Broydens update:" << endl << endl;

dataB << "Rosenbrock" << endl;
startX={5,-5};
iterations = NewtonsMinimizationBroyden(functionRB,functionRBgrad,startX,tolerence);
dataB << startX << endl << "iterations= " << iterations << endl << endl;

dataB << "Himmelblau" << endl;
startX={5,-5};
iterations = NewtonsMinimizationBroyden(functionHB,functionHBgrad,startX,tolerence);
dataB << startX << endl << "iterations= " << iterations << endl << endl;

dataB <<"The following are the minima of the rosenbrock and himmelblau function found using newtons minimization method from part A:" << endl << endl;

dataB << "Rosenbrock" << endl;
startX={5,-5};
iterations = NewtonsMinimization(functionRB,functionRBgrad,functionRBhess,startX,tolerence);
dataB << startX << endl << "iterations= " << iterations << endl << endl;

dataB << "Himmelblau" << endl;
startX={5,-5};
iterations = NewtonsMinimization(functionHB,functionHBgrad,functionHBhess,startX,tolerence);
dataB << startX << endl << "iterations= " << iterations << endl << endl;

dataB << "The following are the minima of the rosenbrock and himmelblau function found using root finding on the gradient:" << endl << endl;

dataB << "Rosenbrock" << endl;
startX={5,-5};
iterations = NewtonsMethod(functionRBgrad,startX,1e-4,tolerence);
dataB << startX << endl << "iterations= " << iterations << endl << endl;

dataB << "Himmelblau" << endl;
startX={5,-5};
iterations = NewtonsMethod(functionHBgrad,startX,1e-4,tolerence);
dataB << startX << endl << "iterations= " << iterations << endl << endl;


dataB << endl << "The stating point for all the above functions is (5,-5). It seems that the function from Part A is the fastest. Root finding method is also quite good, while Broyden is a bit behind...." << endl << endl << "Now for the decay part:";

cout << endl << "Using Broyden:" << endl;
startX = {5,5,5};
iterations = NewtonsMinimizationBroyden_numgrad(exp_decay,startX,tolerence);
dataB << "The function coefficients are:" << endl << startX << endl << "iterations:" << iterations << endl << "see plot for figure";

ofstream data_decay;
data_decay.open("data.decay.txt");
int data_points=500;
double start = t(0), end=t(N-1);
double dx = (end-start)/data_points;
double time=start;
for (int i=0;i<data_points;i++){
	if(i<N){
	data_decay << time << "\t" << startX(0)*exp(-time/startX(1))+startX(2) << "\t" << t(i) << "\t" << y(i) << "\t" << e(i) << "\n";
	}
	else{
	data_decay << time << "\t" << startX(0)*exp(-time/startX(1))+startX(2) << "\n";
	}
	time+=dx;
}


data_decay.close();
dataB.close();


return 0;
}
