#include "optimization.hh"
#include "qr.hh"

vec num_gradient(double f(vec x), vec x, double dx){
	int n = x.n_elem;
	vec df(n);
	double fx = f(x);
	for(int i=0;i<n;i++){
		x(i) += dx;
		df(i) = (f(x)-fx)/dx;
		x(i) -= dx;
	}
	return df;
}

	//class qr_gs qr(A,R); 
	//qr.solve(A,R,b,x); ARx=B
int NewtonsMinimization(double f(vec x), vec gradient(vec x), mat hessian(vec x),vec &x, double tolerence){
	int iterations = 0;
	int n = x.n_elem;//dimensions
	int cont = 1; //continue?
	double alpha = 1e-4; //As  described on backtracking.
	mat H(n,n); 
	mat R(n,n); //for QR decom
	vec Dx(n);  //for QR solve
	vec y(n), fy(n), df(n), fx(n); 
	



	while(cont==1){
		iterations++;
		fx=f(x);
		H=hessian(x);
		df=gradient(x);
		
		//Solve H(x)Dx = -df(x) as in eq 5.
		vec minus_df = (-1)*df;
		class qr_gs qr(H,R); //decomposition to QR
		qr.solve(H,R,minus_df,Dx); //solving


		//This seems to be the backtracking part
		double bt = 2;
		int    bt_cont = 1;
		while(bt_cont==1){
			bt/=2.0;
			y=x+Dx*bt;
			fy=f(y);
			if( norm(fy) < (norm(fx)+alpha*bt*dot(Dx,df)) || bt<0.02){bt_cont=0;} //first is the armijo condition
			}
		x=y;
		fx=fy;		
		if(norm(df)<tolerence){cont=0;} //stop if at extremum
	}
return iterations;	
}

int NewtonsMinimizationBroyden(double f(vec x), vec gradient(vec x), vec &x, double tolerence){
	int iterations = 0;
	int n = x.n_elem;
	int cont=1;
	double alpha = 1e-4;
	mat Hinv(n,n);
	Hinv=eye(n,n);
	vec df = gradient(x);
	double fx = f(x);
	double fy;
	vec y(n), Dx(n), s(n), y_broy(n); //Last is the y defined in the text

	while(cont==1){
		iterations++;
		Dx = (-1.0)*Hinv*df; //Same as above but we already have the hessian inverse
		double  bt = 2;
		int bt_cont= 1;
		
		while(bt_cont==1){
			bt/=2.0;
			s = Dx*bt;
			y=x+s;
			fy=f(y);
			if(norm(fy) < (norm(fx)+alpha*bt*dot(Dx,df))){bt_cont=0;}
			if(norm(s) < 0.00002){Hinv=eye(n,n); bt_cont=0;}	
		}
		//Update as done in the text
		y_broy = gradient(x+s)-gradient(x); 
		Hinv+= ((s-Hinv*y_broy)*s.t()*Hinv)/(dot(y_broy,Hinv*s));
		x=y; fx=fy; df=gradient(y);
		if(norm(df) < tolerence){cont=0;}
	}
	return iterations;
}


int NewtonsMinimizationBroyden_numgrad(double f(vec x), vec &x, double tolerence){
	int iterations = 0;
	int n = x.n_elem;
	int cont=1;
	double alpha = 1e-4;
	double dx = 1e-6; //for the gradient
	mat Hinv(n,n);
	Hinv=eye(n,n);
	vec df = num_gradient(f,x,dx);
	double fx = f(x);
	double fy;
	vec y(n), Dx(n), s(n), y_broy(n); //Last is the y defined in the text

	while(cont==1){
		iterations++;
		Dx = (-1.0)*Hinv*df; //Same as above but we already have the hessian inverse
		double  bt = 2;
		int bt_cont= 1;
		
		while(bt_cont==1){
			bt/=2.0;
			s = Dx*bt;
			y=x+s;
			fy=f(y);
			if(norm(fy) < (norm(fx)+alpha*bt*dot(Dx,df))){bt_cont=0;}
			if(norm(s) < 0.02){Hinv=eye(n,n); bt_cont=0;}	
		}
		//Update as done in the text
		y_broy = num_gradient(f,x+s,dx)-num_gradient(f,x,dx); 
		Hinv+= ((s-Hinv*y_broy)*s.t()*Hinv)/(dot(y_broy,Hinv*s));
		x=y; fx=fy; df=num_gradient(f,y,dx);
		if(norm(df) < tolerence){cont=0;}
	}
	return iterations;
}




int NewtonsMethod(vec f(vec x), vec &x, double stepsize, double tolerence){
	int n = x.n_elem;//dimensions
	int iterations = 0;
        int cont = 1; //continue?
        mat J(n,n); //for jacobian
	mat R(n,n); //for QR 
	vec Dx(n);  //for QR solve
	vec y(n), fy(n), df(n), fx(n);
	while(cont==1){
		iterations++;
        	fx=f(x);
		//J-matrix calculated
		for(int j=0;j<n;j++){
                	x(j)+=stepsize;
			df=f(x)-fx;
    	                for(int i=0;i<n;i++){
	  	        	J(i,j)=df(i)/stepsize;
                     	}
			x(j)-=stepsize;
                }
    		//Solve J Dx = -f(x) as in eq 5.
                vec minus_fx = (-1)*fx;
                class qr_gs qr(J,R); //decomposition to QR
                qr.solve(J,R,minus_fx,Dx); //solving 
                //This seems to be the backtracking part
                double bt = 2;
                int    bt_cont = 1;
                while(bt_cont==1){ 
      			bt/=2.0;
		        y=x+Dx*bt;
                        fy=f(y);
			if(norm(fy)<((1.0-bt/2.0)*norm(fx)) || bt<0.02){bt_cont=0;}
                        }
	                x=y;
  	                fx=fy;          	
			if(norm(Dx)<stepsize || norm(fx)<tolerence){cont=0;}
			}     
	return iterations;
}
