#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>

#ifndef optim_guard
#define optim_guard

//using namespace std;
using namespace arma;


int NewtonsMinimization(double f(vec x), vec gradient(vec x), mat hessian(vec x),vec &x, double tolerence);

int NewtonsMinimizationBroyden(double f(vec x), vec gradient(vec x), vec &x, double tolerence);

int NewtonsMinimizationBroyden_numgrad(double f(vec x), vec &x, double tolerence);

vec num_gradient(double f(vec &x), vec& x, double dx);
	
int NewtonsMethod(vec f(vec x), vec &x, double stepsize, double tolerence);
#endif
