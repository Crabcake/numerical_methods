#include "qr.hh"
#include "root_finding.hh"

//Functions used. I have both A and B in this file. 
int runs; //allows calculating the amount of runs of each function

vec functionA1(vec X){
	runs++;
	double A=1000;
	double x = X(0);
	double y = X(1);
	vec fX(2);
	fX(0) = A*x*y-1.0;
	fX(1) = 1+1.0/A-exp(-x)-exp(-y);
	return fX;
};

mat functionA1Jac(vec X){
	runs++;
	double A=1000;
	double x = X(0);
	double y = X(1);
	mat J(2,2);
	J(0,0) = A*y;
	J(0,1) = A*x;
	J(1,1) = exp(-y);
	J(1,0) = exp(-x);
	return J;
}


vec functionRB(vec X){
	runs++;
	double x = X(0);
	double y = X(1);
	vec fX(2);
	fX(0) = 2*(200*x*x*x -200*x*y+x-1);// df/dx
	fX(1) = 200*(y-x*x); // df/dy
	return fX;
};

mat functionRBJac(vec X){
	runs++;
	double x = X(0);
	double y = X(1);
	mat J(2,2);
	J(0,0) = 2*(600*x*x-200*y+1);
	J(0,1) = -400*x;
	J(1,1) = 200;
	J(1,0) = -400*x;
	return J;
}


vec functionHB(vec X){
	runs++;
	double x = X(0);
	double y = X(1);
	vec fX(2);
	fX(0) = 2.0*(2.0*x*(x*x+y-11)+x+y*y-7);// df/dx
	fX(1) = 2.0*(x*x+2*y*(x+y*y-7)+y-11); // df/dy
	return fX;
};

mat functionHBJac(vec X){
	runs++;
	double x = X(0);
	double y = X(1);
	mat J(2,2);
	J(0,0) = 12*x*x+4*y-42;
	J(0,1) = 4*x+4*y;
	J(1,1) = 4*x+12*y*y-26;
	J(1,0) = 4*x+4*y;
	return J;
}


vec functionTHCF(vec X){ //Three Hump Camel Function
	runs++;
	double x = X(0);
	double y = X(1);
	vec fX(2);
	fX(0) = x*x*x*x*x-4.2*x*x*x+4*x+y;// df/dx
	fX(1) = x+2*y; // df/dy
	return fX;
};


mat functionTHCFJac(vec X){
	runs++;
	double x = X(0);
	//double y = X(1);
	mat J(2,2);
	J(0,0) = 5*x*x*x*x-3*4.2*x*x+4;
	J(0,1) = 1.0;
	J(1,1) = 2.0;
	J(1,0) = 1.0;
	return J;
}

//Excercise A (and B)
int main(){

double tolerence=1e-3;
double stepsize =1e-6;
vec startX(2);
ofstream dataA;
dataA.open("dataAB.txt");
cout << "See the data file 'DataAB.txt' for data for excercise A and B" << endl;

dataA << "The following are four functions run both with and without analytical jacobian for excercise A and B" << endl;

//A1
dataA << endl << "The first function which is to be solved:" << endl;
runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethod(functionA1, startX, stepsize, tolerence);
dataA << endl << "Solution Coordinates without anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl; 

runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethodJacobian(functionA1, functionA1Jac, startX, tolerence);
dataA << endl << "Solution Coordinates with anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl << endl; 

//Rosenbrock
dataA << endl << "Rosenbrock function:" << endl;
runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethod(functionRB, startX, stepsize, tolerence);
dataA << endl << "Solution Coordinates without anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl; 

runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethodJacobian(functionRB, functionRBJac, startX, tolerence);
dataA << endl << "Solution Coordinates with anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl << endl; 




//Himmelblau
dataA << endl << "Himmelblau function:" << endl;
runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethod(functionHB, startX, stepsize, tolerence);
dataA << endl << "Solution Coordinates without anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl; 

runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethodJacobian(functionHB, functionHBJac, startX, tolerence);
dataA << endl << "Solution Coordinates with anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl << endl;






//Thee hump camel function
dataA << endl << "Three hump camel function:" << endl;
runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethod(functionTHCF, startX, stepsize, tolerence);
dataA << endl << "Solution Coordinates without anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl; 

runs = 0; 
startX(0) = 5.0;
startX(1) = -5.0;
NewtonsMethodJacobian(functionTHCF, functionTHCFJac, startX, tolerence);
dataA << endl << "Solution Coordinates with anal. jacobian" << endl << startX << endl << "Functions calls = " << runs << endl; 



dataA << "The analytical jacobian is slightly more effeicient with respect to the amount of function calls";





dataA.close();


return 0;
}
