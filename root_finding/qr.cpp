#include "qr.hh"
#include <armadillo>
#include <iostream>
#include <assert.h>

using namespace std;
using namespace arma;


qr_gs::qr_gs(Mat<double>& A, Mat<double>& R) 
:n(A.n_rows), m(A.n_cols) 
{//decomp of a square matrix this time. There might be an issue with the other version used in the previous excercises....
	for(size_t i = 0; i < A.n_cols; i++){
		R(i,i) = sqrt(dot(A.col(i),A.col(i)));
		A.col(i) /= sqrt(dot(A.col(i),A.col(i)));
	
		for(size_t j=i+1;j<A.n_cols;j++){
			double s = dot( A.col(i),A.col(j) );
			A.col(j)-= s*A.col(i);
			R(i,j)=s;
		}
	}
}

void qr_gs::solve(Mat<double> Q, Mat<double> R, Col<double>& b, Col<double>& x) //This is probably not optimal but i am having trouble accessing A and R from the contructor. So hotfix....
{
int m=R.n_rows;
vec c = Q.t()*b;
for (int i=(m-1);i>=0;i--){

	x(i) = c(i);
	for (int k=i+1;k<m;k++){
		x(i)-=R(i,k)*x(k);
	}
	x(i) /= R(i,i);
}
}

void qr_gs::inverse(Mat<double>& B,Mat<double> Q, Mat<double> R)
{
B = B.eye(); //Setting B to start as identity matrix
for (int i = 0;i<n;i++)
{
	vec b_i=B.col(i);  
	solve(Q,R,b_i,b_i);  //Check this. I modified the above backsub but that changed this one too. It might not function as it should..
	//cout << "\nThis should be unit vector\n" << Q*R*b_i;
	B.col(i) = b_i;
}
//cout << Q*R*B;
}
