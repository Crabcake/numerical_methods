#include <iostream>
#include <armadillo>

#ifndef qr_guard
#define qr_guard

using namespace std;
using namespace arma;

class qr_gs{
	
	public:
		int n,m;
		qr_gs(Mat<double>& A, Mat<double>& R);
		void solve(Mat<double> Q, Mat<double> R,Col<double>& b, Col<double>& x);
		void inverse(Mat<double>& B,Mat<double> Q, Mat<double> R); 
	private:

	protected:

};

#endif
