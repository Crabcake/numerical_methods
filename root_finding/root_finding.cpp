#include "root_finding.hh"
#include "qr.hh"


	//class qr_gs qr(A,R); 
	//qr.solve(b,A,R,c);
void NewtonsMethod(vec f(vec x), vec &x, double stepsize, double tolerence){
	int n = x.n_elem;//dimensions
	int cont = 1; //continue?
	mat J(n,n); //for jacobian
	mat R(n,n); //for QR decom
	vec Dx(n);  //for QR solve
	vec y(n), fy(n), df(n), fx(n); 
	



	while(cont==1){
		fx=f(x);
		//J-matrix calculated
		for(int j=0;j<n;j++){
			x(j)+=stepsize;
			df=f(x)-fx;
			for(int i=0;i<n;i++){
				J(i,j)=df(i)/stepsize;
			}
			x(j)-=stepsize;
		}
	

		//Solve J Dx = -f(x) as in eq 5.
		vec minus_fx = (-1)*fx;
		class qr_gs qr(J,R); //decomposition to QR
		qr.solve(J,R,minus_fx,Dx); //solving


		//This seems to be the backtracking part
		double bt = 2;
		int    bt_cont = 1;
		while(bt_cont==1){
			bt/=2.0;
			y=x+Dx*bt;
			fy=f(y);
			if(norm(fy)<((1.0-bt/2.0)*norm(fx)) || bt<0.02){bt_cont=0;}
			}
		x=y;
		fx=fy;		
		if(norm(Dx)<stepsize || norm(fx)<tolerence){cont=0;}
	}
	
}


void NewtonsMethodJacobian(vec f(vec x), mat j(vec x), vec &x, double tolerence){
	int n = x.n_elem;//dimensions
	int cont = 1; //continue?
	mat J(n,n); //for jacobian
	mat R(n,n); //for QR decom
	vec Dx(n);  //for QR solve
	vec y(n), fy(n), df(n), fx(n); 
	



	while(cont==1){
		fx=f(x);
		J = j(x);	

		//Solve J Dx = -f(x) as in eq 5.
		vec minus_fx = (-1)*fx;
		class qr_gs qr(J,R); //decomposition to QR
		qr.solve(J,R,minus_fx,Dx); //solving


		//This seems to be the backtracking part
		double bt = 2;
		int    bt_cont = 1;
		while(bt_cont==1){
			bt/=2.0;
			y=x+Dx*bt;
			fy=f(y);
			if(norm(fy)<((1.0-bt/2.0)*norm(fx)) || bt<0.02){bt_cont=0;}
			}
		x=y;
		fx=fy;		
		if(norm(fx)<tolerence){cont=0;}
	}
	
}

