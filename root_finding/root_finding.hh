#include <iostream>
#include <vector>
#include <armadillo>
#include <functional>

#ifndef rf_guard
#define rf_guard

//using namespace std;
using namespace arma;

void NewtonsMethod(vec f(vec x), vec &x, double stepsize, double tolerence);
void NewtonsMethodJacobian(vec f(vec x), mat j(vec x), vec &x, double tolerence);

#endif
